﻿using DAL.Interface;
using DAL.Model.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class AdminSectionService : IAdminSectionService
    {
        private readonly IAdminSectionRepo _adminSectionRepo;
        public AdminSectionService(IAdminSectionRepo adminSectionRepo)
        {
            _adminSectionRepo = adminSectionRepo;
        }

        public MasterCompany Add(MasterCompany model)
        {
            return _adminSectionRepo.Add(model);
        }

        public async Task<Menu> AddMenu(Menu model)
        {
            return await _adminSectionRepo.AddMenu(model);
        }

        public Navbar AddNavbar(Navbar model)
        {
            return _adminSectionRepo.AddNavbar(model);
        }

        public MasterCompany Delete(int Id)
        {
            return _adminSectionRepo.Delete(Id);
        }

        public async Task<Menu> DeleteMenuById(int Id)
        {
            return await _adminSectionRepo.DeleteMenuById(Id);
        }

        public Navbar DeleteNavbarById(int Id)
        {
            return _adminSectionRepo.DeleteNavbarById(Id);
        }

        public IEnumerable<MasterCompany> GetAll()
        {
            return _adminSectionRepo.GetAll();
        }

        public IEnumerable<Menu> GetAllMenu()
        {
            return _adminSectionRepo.GetAllMenu();
        }

        public IEnumerable<Navbar> GetAllNavbar()
        {
            return _adminSectionRepo.GetAllNavbar();
        }

        public MasterCompany GetById(int Id)
        {
            return _adminSectionRepo.GetById(Id);
        }

        public async Task<Menu> GetMenuById(int Id)
        {
            return await _adminSectionRepo.GetMenuById(Id);
        }

        public Navbar GetNavbarById(int Id)
        {
            return _adminSectionRepo.GetNavbarById(Id);
        }

        public MasterCompany Update(MasterCompany model)
        {
            return _adminSectionRepo.Update(model);
        }

        public async Task<Menu> UpdateMenu(Menu model)
        {
            return await _adminSectionRepo.UpdateMenu(model);
        }

        public Navbar UpdateNavbar(Navbar model)
        {
            return _adminSectionRepo.UpdateNavbar(model);
        }
    }
}
