﻿using AutoMapper;
using DAL.Model.Entity;
using Service.MapModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Helper
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<Menu, MenuModel>(); // means you want to map from Menu to MenuModel
            CreateMap<Menu, MenuModel>().ReverseMap(); // means you want to map from Menu to MenuModel
        }
    }
}
