﻿using DAL.Interface;
using DAL.Model;
using DAL.Model.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Infrastructure
{
    public class AdminSectionRepo : IAdminSectionRepo
    {
        private readonly AppDbContext context;

        public AdminSectionRepo(AppDbContext context)
        {
            this.context = context;
        }

        public MasterCompany Add(MasterCompany model)
        {
            context.masterCompany.Add(model);
            context.SaveChanges();
            return model;
        }

        public async Task<Menu> AddMenu(Menu model)
        {
            await context.menu.AddAsync(model);
            await context.SaveChangesAsync();
            return model;
        }

        public Navbar AddNavbar(Navbar model)
        {
            context.navbar.Add(model);
            context.SaveChanges();
            return model;
        }

        public MasterCompany Delete(int Id)
        {
            MasterCompany masterCompany = context.masterCompany.Find(Id);
            if (masterCompany != null)
            {
                context.masterCompany.Remove(masterCompany);
                context.SaveChanges();
            }
            return masterCompany;
        }

        public async Task<Menu> DeleteMenuById(int Id)
        {
            Menu menu = context.menu.Find(Id);
            if (menu != null)
            {
                context.menu.Remove(menu);
                await context.SaveChangesAsync();
            }
            return menu;
        }

        public Navbar DeleteNavbarById(int Id)
        {
            Navbar menu = context.navbar.Find(Id);
            if (menu != null)
            {
                context.navbar.Remove(menu);
                context.SaveChanges();
            }
            return menu;
        }

        public IEnumerable<MasterCompany> GetAll()
        {
            return context.masterCompany;
        }

        public IEnumerable<Menu> GetAllMenu()
        {
            var list = context.menu.Where(x => x.IsActive == true);
            return list;
        }

        public IEnumerable<Navbar> GetAllNavbar()
        {
            var list = context.navbar.Where(x => x.IsActive == true).ToList();
            return list;
        }

        public MasterCompany GetById(int Id)
        {
            return context.masterCompany.Find(Id);
        }

        public async Task<Menu> GetMenuById(int Id)
        {
            return await context.menu.FindAsync(Id);
        }

        public Navbar GetNavbarById(int Id)
        {
            return context.navbar.Find(Id);
        }

        public MasterCompany Update(MasterCompany model)
        {
            var masterCompany = context.masterCompany.Attach(model);
            masterCompany.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
            return model;
        }

        public async Task<Menu> UpdateMenu(Menu model)
        {
            var menu = context.menu.Attach(model);
            menu.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            await context.SaveChangesAsync();
            return model;
        }

        public Navbar UpdateNavbar(Navbar model)
        {
            var menu = context.navbar.Attach(model);
            menu.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
            return model;
        }
    }
}