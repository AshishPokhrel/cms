﻿using DAL.Model.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Model
{
   public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        public DbSet<MasterCompany> masterCompany { get; set; }
        public DbSet<Menu> menu { get; set; }
        public DbSet<Navbar> navbar { get; set; }
    }
}
