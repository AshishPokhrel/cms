﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Model.Entity
{
   public class Navbar
    {
        public int Id { get; set; }
        public int MasterCompanyId { get; set; }
        public string Title { get; set; }
        public string ColorName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime UpdatedOn { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
    }
}