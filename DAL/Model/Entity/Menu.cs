﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Model.Entity
{
   public class Menu
    {
        public int Id { get; set; }
        public int OrderNumber { get; set; }
        public int MasterCompanyId { get; set; }
        public int ParentId { get; set; }
        public string Title { get; set; }
        public string ControllerName { get; set; }
        public string Action { get; set; }
        public string ClassName { get; set; }
        public string IconName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime UpdatedOn { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid UpdatedBy { get; set; }
    }
}
