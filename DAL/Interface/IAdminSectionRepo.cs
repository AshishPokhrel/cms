﻿using DAL.Model.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interface
{
    public interface IAdminSectionRepo
    {
        #region MasterCompany
        MasterCompany GetById(int Id);
        IEnumerable<MasterCompany> GetAll();
        MasterCompany Add(MasterCompany model);
        MasterCompany Update(MasterCompany model);
        MasterCompany Delete(int Id);
        #endregion

        #region menu
        Task<Menu> GetMenuById(int Id);
        IEnumerable<Menu> GetAllMenu();
        Task<Menu> AddMenu(Menu model);
        Task<Menu> UpdateMenu(Menu model);
        Task<Menu> DeleteMenuById(int Id);
        #endregion

        #region navbar
        Navbar GetNavbarById(int Id);
        IEnumerable<Navbar> GetAllNavbar();
        Navbar AddNavbar(Navbar model);
        Navbar UpdateNavbar(Navbar model);
        Navbar DeleteNavbarById(int Id);
        #endregion
    }
}
