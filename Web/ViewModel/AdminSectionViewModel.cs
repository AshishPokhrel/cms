﻿using Service.MapModel.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Web.ViewModel
{
    public class AdminSectionViewModel
    {
        public NavbarModel navbarModel { get; set; }
        public IEnumerable<MenuModel> menuModelList { get; set; }
        public MasterCompanyModel masterCompanyModel { get; set; }
    }
 
}