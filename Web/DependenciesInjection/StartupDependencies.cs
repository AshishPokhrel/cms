﻿using DAL.Infrastructure;
using DAL.Interface;
using Microsoft.Extensions.DependencyInjection;
using Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.DependenciesInjection
{
    public class StartupDependencies
    {
        public static void RegisterServices(IServiceCollection services)
        {
            RegisterGeneralServiceAndRepositories(services);
        }
        public static void RegisterGeneralServiceAndRepositories(IServiceCollection services)
        {
            //Register Repositories Related to Resources. These repositories are Default 
            //implementations to provide queries to Resource and Associated Tables
            services.AddScoped<IAdminSectionRepo, AdminSectionRepo>();
            services.AddScoped<IAdminSectionService, AdminSectionService>();

        }
    }
}
