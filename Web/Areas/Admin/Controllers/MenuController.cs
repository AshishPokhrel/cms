﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.Model;
using DAL.Model.Entity;
using Service.Services;
using Service.MapModel.Models;
using AutoMapper;
using Web.ViewModel;

namespace Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class MenuController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IAdminSectionService _adminSectionService;

        public MenuController(IAdminSectionService adminSectionService, IMapper mapper)
        {
            _adminSectionService = adminSectionService;
            _mapper = mapper;
        }

        // GET: Admin/Menus
        public IActionResult Index()
        {
            var listModel = new AdminSectionViewModel();
            var model = _adminSectionService.GetAllMenu().ToList();
            var viewModel = _mapper.Map<List<MenuModel>>(model);
            listModel.menuModelList = viewModel;
            return View(listModel);
        }

        public async Task<IActionResult> Details(int id)
        {
            var menu = await _adminSectionService.DeleteMenuById(id);
            if (menu == null)
            {
                return NotFound();
            }

            return View(menu);
        }

        // GET: Admin/Menus/Create
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,OrderNumber,MasterCompanyId,ParentId,Title,ControllerName,Action,ClassName,IconName,IsActive,IsDeleted,UpdatedOn,CreatedBy,UpdatedBy")] Menu menu)
        {
            if (ModelState.IsValid)
            {
                await _adminSectionService.AddMenu(menu);
                return RedirectToAction(nameof(Index));
            }
            return View(menu);
        }

        // GET: Admin/Menus/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var menu = await _adminSectionService.GetMenuById(id);
            if (menu == null)
            {
                return NotFound();
            }
            return View(menu);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,OrderNumber,MasterCompanyId,ParentId,Title,ControllerName,Action,ClassName,IconName,IsActive,IsDeleted,UpdatedOn,CreatedBy,UpdatedBy")] Menu menu)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _adminSectionService.UpdateMenu(menu);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MenuExists(menu.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(menu);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var menu = await _adminSectionService.GetMenuById(id);
            if (menu == null)
            {
                return NotFound();
            }

            return View(menu);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var menu = await _adminSectionService.DeleteMenuById(id);
            return RedirectToAction(nameof(Index));
        }

        private bool MenuExists(int id)
        {
            var result = _adminSectionService.GetMenuById(id);
            if (result != null)
                return true;
            else
                return false;
        }
    }
}
