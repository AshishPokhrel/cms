﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Service.Services;

namespace Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class MasterCompanyController : Controller
    {
        private readonly IAdminSectionService _adminSectionService;

        public MasterCompanyController(IAdminSectionService adminSectionService)
        {
            _adminSectionService = adminSectionService;
        }

        public IActionResult Index()
        {
           var list = _adminSectionService.GetAll().ToList();
            return View(list);
        }
    }
}